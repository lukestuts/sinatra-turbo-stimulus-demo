#!/usr/bin/ruby

# Demonstrate Turbo and Stimulus 2.0 using Sinatra, Puma and Server-Sent Events

require 'sinatra'
require 'sinatra/partial'
require 'json'

use Rack::Deflater
set :partial_template_engine, :erb
set connections: []
set items: [
  { id: 1, text: "My first item" },
  { id: 2, text: "My second item" },
  { id: 3, text: "My third item" },
  { id: 4, text: "My fourth item" },
]

module Sinatra
  class Request
    def turbo_stream?
      self.env['HTTP_ACCEPT'].include?('text/vnd.turbo-stream.html')
    end
    def turbo_frame_id
      self.env['HTTP_TURBO_FRAME']
    end
  end
end

get '/' do
  erb :index
end

get '/title_frame' do
  content_type "text/html"
  return [
    "<turbo-frame id='#{request.turbo_frame_id}'>",
    "<h1>Turbo and Simulus 2.0 Demo using Sinatra and Server-Sent Events</h1>",
    "</turbo-frame>"
  ].join()
end

get '/turbo_stream' do
  request.env['rack.hijack'].call
  stream = request.env['rack.hijack_io']
  headers = [
    "HTTP/1.1 200 OK",
    "Content-Type: text/event-stream"
  ]
  stream.write(headers.map { |header| header + "\r\n" }.join)
  stream.write("\r\n")
  stream.flush
  settings.connections << stream
  response.close
end

def create_item(text)
  new_id = settings.items.map { |item| item[:id] }.max.to_i + 1
  new_item = { id: new_id, text: text }
  settings.items << new_item
  turbo_stream(action: 'append', target: 'items',
               content: partial(:item, collection: [ new_item ]))
end

post '/items/create' do
  text = JSON.parse(request.body.read)['text']
  create_item(text)
  response.close()
end

def delete_item(id)
  item = settings.items.select { |item| item[:id] == id}.first
  settings.items.delete(item)
  element_id = "item_#{id}"
  turbo_stream(action: 'remove', target: "item_#{id}")
end

post '/items/delete' do
  id = JSON.parse(request.body.read)['id']
  delete_item(id)
  response.close()
end

def turbo_stream(action: '', target: '', content: '')
  template_needed = (action != 'remove')
  to_delete = []
  settings.connections.each do |connection|
    begin
      update = []
      update <<  "<turbo-stream action='#{action}' target='#{target}'>"
      update << '<template>' if template_needed
      update << content.gsub("\r", "").gsub("\n", "")
      update << '</template>' if template_needed
      update <<  '</turbo-stream>'
      connection.write("data: #{update.join()}\n\n")
    rescue Errno::EPIPE # Connection closed
      to_delete << connection
    rescue Exception => e
      puts "Error streaming: #{e.class} #{e.message} #{e.backtrace.join("\n")}"
    end
  end
  to_delete.each { |connection| settings.connections.delete(connection) }
end

update_time_thread = Thread.new do
  loop do
    turbo_stream(action: 'replace', target: 'time-now',
                 content: "<span id='time-now'>#{Time.now.to_s}</span>")
    sleep 1
  end
end
